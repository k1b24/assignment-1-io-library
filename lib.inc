section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    	cmp byte [rdi + rax], 0
	je .end
	inc rax
	jmp .loop
    .end:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push 0
    push rdi
    mov rdi, rsp
    call print_string
    pop rax
    pop rax
    xor rax, rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; Выводит знаковое 8-байтовое число в десятичном формате 
print_uint:
    mov rax, rdi
    mov r10, rsp
    push 0
    .loop:
        call dividebyten
        add rdx, '0' 
        dec rsp
        mov [rsp], dl
        cmp rax, 0
        jne .loop
    mov rdi, rsp
    call print_string
    mov rsp, r10
    ret

dividebyten:        
	xor rdx, rdx
        mov r8, 10
        cmp rax, 10
        jb .belowten
        div r8
        ret
        .belowten:
                mov rdx, rax
                mov rax, 0
                ret


print_int:
    xor rax, rax
	cmp rdi, 0
	jnl .call_print_uint
	mov r8, rdi
	mov rdi, '-' 
	call print_char
	mov rdi, r8
	neg rdi	
	.call_print_uint:
		jmp print_uint	


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	push rdi
	push rsi
    call string_length
	pop rsi
	pop rdi
    mov r8, rax
    mov r9, rdi
    mov rdi, rsi
    call string_length
    mov r10, rax
    mov rdi, r9	
    cmp r8, r10
    jne .not_equals
    mov rax, 0
    .loop:
		mov r9b, byte [rdi + rax]
		cmp byte [rsi + rax], r9b 
		jne .not_equals
		cmp rax, r8
		je .equals
		inc rax
		jmp .loop
    .equals:
		mov rax, 1
		ret
    .not_equals:
		mov rax, 0
		ret





; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rdi, 0
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi
    mov r10, r8
    mov rcx, 0
    cmp rsi, 0
    je .end
    .loop_until_not_space:
       push rcx
       call read_char
       pop rcx
       mov rdi, r10
       mov rsi, r9
       cmp rax, ' '
       je .loop_until_not_space
       cmp rax, '	'
       je .loop_until_not_space
    .mainloop:
       cmp rax, 0
       je .add_null_and_end
       cmp rax, ' '
       je .add_null_and_end
       mov byte [r8], al        
       inc r8
       inc rcx
       cmp rcx, rsi
       je .end
       push rcx
       call read_char
       pop rcx
       mov rdi, r10
       mov rsi, r9
       jmp .mainloop
    .add_null_and_end:
       mov byte [r8], 0
       mov rax, r10
       mov rdx, rcx
       ret
    .end:
       mov rax, 0
       ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r9, r9
    xor r10, r10
    mov r11, 10
	push r12
	xor r12, r12
    .loop:
		xor rax, rax
        mov al, byte [rdi]
		cmp rax, '0'
        jb .eol
        cmp rax, '9'
        ja .eol
        sub rax, '0'
		mov r10, rax
		mov rax, r9
		mul r11
        add rax, r10
		mov r9, rax
		inc rdi
		inc r12
        jmp .loop
    .eol:
        mov rax, r9
		mov rdx, r12
		pop r12
        ret
		



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	xor rax, rax
    mov al, byte[rdi]
	cmp rax, '-'
	je .negative
	call parse_uint
	ret
	.negative:
		inc rdi
		call parse_uint
		neg rax
		inc rdx		
		ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
	push rdi
	push rsi
	push rdx
    call string_length
	pop rdx
	pop rsi
	pop rdi
    cmp rax, rdx
    ja .not_equals
    mov rcx, 0
    .loop:
		mov r8b, byte [rdi + rcx]
		mov byte [rsi + rcx], r8b
		cmp rcx, rax
		je .end
		inc rcx
		jmp .loop
    .end:
		ret
    .not_equals:
		mov rax, 0
		ret  
